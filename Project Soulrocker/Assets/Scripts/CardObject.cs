﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class CardObject : GridObject
{
    public enum Context
    {
        inGame,
        selectedForDeckBuilding,
        showingForDeckBulding
    }

    public Image cardImage;
    public Image typeBanner;
    public Image numBacker;
    public Image imageBacker;
    public TextMeshProUGUI cardName;
    public TextMeshProUGUI cardDescription;
    public TextMeshProUGUI cardType;
    public TextMeshProUGUI cardNum;
    public TextMeshProUGUI cardCost;

    Card card;
    int editedCost;
    float editedNum;
    Card.Type editedType;
    Context currContext;
    CardEffect effect;

    public void SetCard(Card c, int _editedCost, float _editedNum, Card.Type _editedType, Context context = Context.inGame)
    {
        currContext = context;

        card = c;
        cardName.text = c.cardName;
        cardImage.sprite = c.sprite;
        cardCost.text = _editedCost.ToString();
        cardNum.text = _editedNum.ToString();
        editedCost = _editedCost;
        editedNum = _editedNum;
        editedType = _editedType;
        cardDescription.text = c.description;

        effect = CardEffectManager.Instance.GetCardEffect(c.effectEnum);

        ChangeStyleUI(editedType);

        if (c is AttackCard)
        {
            imageBacker.sprite = ColorManager.Instance.attackSprite;
            numBacker.color = ColorManager.Instance.attackColor;

        }
        else if (c is HealCard)
        {
            imageBacker.sprite = ColorManager.Instance.healSprite;
            numBacker.color = ColorManager.Instance.healColor;

        }
        else if (c is BuffCard)
        {
            imageBacker.sprite = ColorManager.Instance.buffSprite;
            numBacker.color = ColorManager.Instance.buffColor;

        }
        else if (c is DefenseCard)
        {
            imageBacker.sprite = ColorManager.Instance.defenseSprite;
            numBacker.color = ColorManager.Instance.defenseColor;

        }
    }

    public void ClickCard()
    {
        switch (currContext)
        {
            case Context.inGame:
                //Clicking cards only works if its the pick cards phase
                if (CardGameManager.Instance.GetPhase() == CardGameManager.Phase.pickCards && transform.parent == CardGameManager.Instance.middleHand.transform)
                {
                    //Put card into hand
                    CardGameManager.Instance.GiveCardToPlayer(card, editedCost, editedNum, editedType);
                    //Remove this card from the middle
                    CardGameManager.Instance.middleHand.RemoveObj(gameObject);
                }
                break;
            case Context.selectedForDeckBuilding:
                FindObjectOfType<SelectedCardsForDeck>().RemoveCard(card, gameObject);
                break;
            case Context.showingForDeckBulding:
                FindObjectOfType<SelectedCardsForDeck>().AddCard(card);
                GetComponent<Animator>().SetTrigger("Click");
                break;
        }
    }

    void ChangeStyleUI(Card.Type type)
    {
        cardType.text = type.ToString().ToUpper();

        switch (type)
        {
            case Card.Type.BASH:
                typeBanner.color = ColorManager.Instance.smashColor;
                break;
            case Card.Type.SLASH:
                typeBanner.color = ColorManager.Instance.slashColor;
                break;
            case Card.Type.THROW:
                typeBanner.color = ColorManager.Instance.shootColor;
                break;
        }
    }

    public void EditCost(int newCost)
    {
        cardCost.GetComponent<Animator>().SetTrigger("Edit");
        editedCost = newCost;
        StartCoroutine(EditUI(cardCost, newCost));
    }

    public void EditNum(float newNum)
    {
        cardNum.GetComponent<Animator>().SetTrigger("Edit");
        editedNum = newNum;
        StartCoroutine(EditUI(cardNum, newNum));
    }

    public void EditType(Card.Type newType)
    {
        //add an animation for this
        editedType = newType;
        ChangeStyleUI(editedType);
    }

    public int GetCost()
    {
        return editedCost;
    }

    public float GetNum()
    {
        return editedNum;
    }

    public Card.Type GetCardType()
    {
        return editedType;
    }

    IEnumerator EditUI(TextMeshProUGUI ui, float num)
    {
        yield return new WaitForSeconds(0.5f);
        ui.text = num.ToString();
    }

    public void CheckCost(int cost)
    {
        if (cost >= editedCost)
            cardCost.color = Color.white;
        else
            cardCost.color = Color.red;
    }

    public void Resize()
    {
        Transform parent = transform.parent;

        transform.localScale = new Vector3(1 / parent.localScale.x, 1 / parent.localScale.y, 1 / parent.localScale.z);
    }

    public CardEffect GetEffect()
    {
        return effect;
    }
}
