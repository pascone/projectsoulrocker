﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class BoostObject : GridObject
{
    public TextMeshProUGUI type;
    public TextMeshProUGUI num;
    public TextMeshProUGUI descLeft;
    public TextMeshProUGUI descRight;
    public Image typeBanner;
    public bool left;

    float multiplier;
    bool isBoost;
    Player player;

    bool glue = false;
    bool bubbleWrap = false;
    bool bananaPeel = false;
    bool mascotCostume = false;
    bool surpriseTrampoline = false;
    bool baseballGlove = false;

    public void SetData(string typeEffected, string textDisplayed, string desc, bool isLeft)
    {
        type.text = typeEffected.ToUpper();
        num.text = textDisplayed;
        descLeft.text = desc;
        descRight.text = desc;
        left = isLeft;

        if (textDisplayed.Contains("x"))
        {
            multiplier = float.Parse(textDisplayed.Replace("x", ""));
            isBoost = true;
        }
        else
        {
            multiplier = float.Parse(textDisplayed.Replace("1/", ""));
            isBoost = false;
        }

        switch (typeEffected.ToLower())
        {
            case "smash":
                typeBanner.color = ColorManager.Instance.smashColor;
                break;
            case "slash":
                typeBanner.color = ColorManager.Instance.slashColor;
                break;
            case "shoot":
                typeBanner.color = ColorManager.Instance.shootColor;
                break;
        }

        GetComponent<Animator>().SetBool("Left", left);
    }

    public void Use()
    {
        GetComponent<Animator>().SetTrigger("Activate");
        GetComponent<Animator>().SetBool("DontDestroy", false);

        if(surpriseTrampoline && player)
        {
            player.DamageEnemyBasedOnNextHit();
        }

        StartCoroutine(Remove());
    }

    public void UseDontDestroy()
    {
        GetComponent<Animator>().SetTrigger("Activate");
        GetComponent<Animator>().SetBool("DontDestroy", true);

        if(surpriseTrampoline && player)
        {
            player.DamageEnemyBasedOnNextHit();
        }
    }

    IEnumerator Remove()
    {
        yield return new WaitForSeconds(2f);
        GetComponentInParent<CardGrid>().RemoveObj(gameObject);
    }

    public void EditType(Card.Type newType)
    {
        type.text = newType.ToString().ToUpper();

        switch (type.text.ToLower())
        {
            case "smash":
                typeBanner.color = ColorManager.Instance.smashColor;
                break;
            case "slash":
                typeBanner.color = ColorManager.Instance.slashColor;
                break;
            case "shoot":
                typeBanner.color = ColorManager.Instance.shootColor;
                break;
        }
    }

    public float GetMultiplier()
    {
        return multiplier;
    }

    public bool CheckIfBoost()
    {
        return isBoost;
    }

    public void GlueObject(Player p)
    {
        player = p;
        glue = true;
    }

    public void BubbleWrapObject(Player p)
    {
        player = p;
        bubbleWrap = true;
    }

    public void MascotCostume(Player p)
    {
        player = p;
        mascotCostume = true;

        EditAllBoosts(1.5f);
    }

    public void SurpriseTrampoline(Player p)
    {
        player = p;
        surpriseTrampoline = true;
    }

    public void BaseballGlove(Player p)
    {
        player = p;
        baseballGlove = true;
    }

    public bool CheckSurpriseTrampoline()
    {
        return surpriseTrampoline;
    }

    void EditAllBoosts(float num)
    {
        BoostObject[] boosts = transform.parent.GetComponentsInChildren<BoostObject>();
        foreach (BoostObject b in boosts)
        {
            if (b.isBoost)
            {
                if (num > 1)
                    b.EditMultiplier(num, Color.yellow);
                else
                    b.EditMultiplier(num, Color.white);
            }
        }
    }

    public void BananaPeelObject()
    {
        bananaPeel = true;
    }

    public void EditMultiplier(float newNums, Color newColor)
    {
        multiplier *= newNums;
        multiplier = Mathf.Round(multiplier * 10) / 10f;
        num.text = "x" + multiplier;
        num.color = newColor;
    }

    private void OnDestroy()
    {
        if (player)
        {
            if (glue)
            {
                if (CardEffectManager.Instance)
                    CardEffectManager.Instance.RemoveEffect(CardEffectEnum.glue, player.playerNum);

            }
            else if (mascotCostume)
            {
                if (CardEffectManager.Instance)
                {
                    CardEffectManager.Instance.RemoveEffect(CardEffectEnum.mascotCostume, player.playerNum);
                    EditAllBoosts(1 / 1.5f);
                }
            }
            else if (surpriseTrampoline)
            {
                if (CardEffectManager.Instance)
                {
                    CardEffectManager.Instance.RemoveEffect(CardEffectEnum.surpriseTrampoline, player.playerNum);
                }
            }
            else if (baseballGlove)
            {
                if (CardEffectManager.Instance)
                {
                    CardEffectManager.Instance.RemoveEffect(CardEffectEnum.baseballGlove, player.playerNum);
                }
            }
        }

        if (bananaPeel)
            if (CardEffectManager.Instance)
                CardEffectManager.Instance.RemoveEffect(CardEffectEnum.bananaPeel, -1);
        else if (bubbleWrap)
                if (CardEffectManager.Instance)
                    CardEffectManager.Instance.RemoveEffect(CardEffectEnum.bubbleWrap, -1);
    }
}
