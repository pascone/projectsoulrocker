﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewAttackCard", menuName = "Card/Attack Card", order = 1)]
public class AttackCard : Card
{
    public override void Activate(Player me, Player enemy, CardObject obj, float optionalNumber)
    {
        float damage = obj.GetNum();
        if (CardEffectManager.Instance.CheckActiveEffect(CardEffectEnum.boomerang, me.playerNum))
        { 
            damage /= 2;
            CardEffectManager.Instance.RemoveEffect(CardEffectEnum.boomerang, me.playerNum);
        }

        int damageNum;
        if (CardEffectManager.Instance.CheckActiveEffect(CardEffectEnum.lunchBox, me.playerNum))
        {
            int healAmount = Mathf.RoundToInt((damage * optionalNumber) - damage);
            damageNum = Mathf.RoundToInt(damage);
            me.EditHealth(healAmount, true);
            CardEffectManager.Instance.RemoveEffect(CardEffectEnum.lunchBox, me.playerNum);
        }
        else
        {
            damageNum = Mathf.RoundToInt(damage * optionalNumber);
        }
        enemy.EditHealth(-damageNum, false);
        me.GetSpecialAbility().EditSpecialAbilityBasedOnDamage(damageNum, false);
        enemy.GetSpecialAbility().EditSpecialAbilityBasedOnDamage(damageNum, true);

        if (CardEffectManager.Instance.CheckActiveEffect(CardEffectEnum.sickle, me.playerNum))
        {
            me.EditHealth(damageNum, true);
            CardEffectManager.Instance.RemoveEffect(CardEffectEnum.sickle, me.playerNum);
        }
    }
}
