﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewHealCard", menuName = "Card/Heal Card", order = 2)]
public class HealCard : Card
{
    public override void Activate(Player me, Player enemy, CardObject obj, float optionalNumber)
    {
        float heal = obj.GetNum();
        if (CardEffectManager.Instance.CheckActiveEffect(CardEffectEnum.boombox, me.playerNum))
        {
            heal /= 2;
            CardEffectManager.Instance.RemoveEffect(CardEffectEnum.boombox, me.playerNum);
        }
        int healNum = Mathf.RoundToInt(heal * optionalNumber);
        me.EditHealth(healNum, true);
        me.GetSpecialAbility().EditSpecialAbilityBasedOnDamage(healNum, false);
    }
}
