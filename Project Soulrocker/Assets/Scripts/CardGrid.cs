﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardGrid : MonoBehaviour
{
    public float xSpawnOffset;                  //The offset in the x axis of where the card will spawn
    public float ySpawnOffset;                  //The offset in the y axis of where the card will spawn
    public float xPosOffset;
    public float yPosOffset;

    List<GameObject> gridObjs;                  //The list of cards this holds

    public float EXPAND_SEPARATION = 2.25f;      //The multiplier for the spacing between cards when a card is expanded
    public bool reverse = false;

    void Awake()
    {
        gridObjs = new List<GameObject>();
    }

    void FixedUpdate()
    {
        UpdateCards();
    }

    public GameObject CreateCard(GameObject obj)
    {
        GameObject g = Instantiate(obj, new Vector3(transform.position.x + xSpawnOffset, transform.position.y + ySpawnOffset, 0), transform.rotation, transform);
        AddObj(g);
        return g;
    }

    //Used in coroutines to lerp the card to its position
    IEnumerator MoveCard(GameObject card, Vector3 newPos)
    {
        while(Vector3.Distance(card.transform.position, newPos) > 0.1f )
        {
            card.transform.position = Vector3.Lerp(card.transform.position, newPos, 0.1f);
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }

    //Add an object to the list
    public void AddObj(GameObject obj)
    {
        //New cards always go at the front of the list to keep the top down order
        gridObjs.Insert(0, obj);
    }

    //Remove an object form the list
    public void RemoveObj(GameObject obj)
    {
        gridObjs.Remove(obj);
        Destroy(obj);
    }

    public void ClearGrid()
    {
        int count = gridObjs.Count;
        for(int i = 0; i < count; i++)
        {
            GameObject g = gridObjs[0];
            gridObjs.RemoveAt(0);
            Destroy(g);
        }
    }

    //Goes through every card in the list and updates their position
    void UpdateCards()
    {
        //Make sure to stop all coroutines before we start more
        StopAllCoroutines();

        //Store the y positon
        float y = transform.position.y;
        //Flag used to check for cards that are being hovered over
        bool hovered = false;

        if (reverse)
        {
            for (int i = gridObjs.Count - 1; i >= 0; i--)
            {
                //Store the card height and scale
                float cardHeight = gridObjs[i].GetComponent<RectTransform>().rect.height;
                float cardScale = gridObjs[i].GetComponent<RectTransform>().lossyScale.y;

                //Normal separation between cards is just height * scale
                float num = (cardHeight * cardScale);

                //If the previous card was being hovered over then increase the separation between it and this card
                if (hovered)
                    num = (cardHeight * EXPAND_SEPARATION * cardScale);

                //Calculate separation
                y -= num;

                //Coroutine to move this card
                StartCoroutine(MoveCard(gridObjs[i], new Vector3(transform.position.x + (xPosOffset * transform.lossyScale.x), y + (yPosOffset * transform.lossyScale.y), 0)));

                //Check if this card is being hovered over
                if (gridObjs[i].GetComponent<GridObject>().GetHovered())
                    hovered = true;
                else
                    hovered = false;
            }
        }
        else
        {
            for (int i = 0; i < gridObjs.Count; i++)
            {
                //Store the card height and scale
                float cardHeight = gridObjs[i].GetComponent<RectTransform>().rect.height;
                float cardScale = gridObjs[i].GetComponent<RectTransform>().lossyScale.y;

                //Normal separation between cards is just height * scale
                float num = (cardHeight * cardScale);

                //If the previous card was being hovered over then increase the separation between it and this card
                if (hovered)
                    num = (cardHeight * EXPAND_SEPARATION * cardScale);

                //Calculate separation
                y -= num;

                //Coroutine to move this card
                StartCoroutine(MoveCard(gridObjs[i], new Vector3(transform.position.x + (xPosOffset * transform.lossyScale.x), y + (yPosOffset * transform.lossyScale.y), 0)));

                //Check if this card is being hovered over
                if (gridObjs[i].GetComponent<GridObject>().GetHovered())
                    hovered = true;
                else
                    hovered = false;
            }
        }
    }

    public List<GameObject> GetObjects()
    {
        return gridObjs;
    }
}
