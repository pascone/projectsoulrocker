﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewBuffCard", menuName = "Card/Buff Card", order = 3)]
public class BuffCard : Card
{
    public override void Activate(Player me, Player enemy, CardObject obj, float optionalNumber)
    {
    }
}
