﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Health : MonoBehaviour
{
    public int maxHealth = 100;      //The max health of this player
    public Slider healthUI;
    public TextMeshProUGUI healthText;
    int currentHealth;         //The current health of this player

    void Start()
    {
        currentHealth = maxHealth;
    }

    //Takes in a number (positive or negative) and adds it to the current health
    //Makes sure currentHealth doesn't go above maxHealth ot below 0
    public bool EditHealth(int num)
    {
        bool flag = false;
        currentHealth += num;
        if(currentHealth <= 0)
        {
            //Die
            currentHealth = 0;
            flag = true;
        }
        else if(currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }

        healthUI.value = currentHealth;
        healthText.text = currentHealth + "/" + maxHealth;
        return flag;
    }

    public void SetHealth(int newHealth)
    {
        currentHealth = newHealth;
        healthUI.value = currentHealth;
        healthText.text = currentHealth + "/" + maxHealth;
    }

    public void SetIncreaseMaxHealth(int num)
    {
        maxHealth += num;
        healthUI.maxValue = maxHealth;
        healthText.text = currentHealth + "/" + maxHealth;
    }

    public int GetMaxHealth()
    {
        return maxHealth;
    }

    public int GetCurrentHealth()
    {
        return currentHealth;
    }
}
