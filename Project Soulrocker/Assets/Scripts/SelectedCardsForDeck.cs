﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SelectedCardsForDeck : MonoBehaviour
{
    public int maxCardAmount = 20;
    public CardGrid selectedCardGrid;
    public GameObject cardPrefab;
    public TextMeshProUGUI cardCount;

    List<Card> cardsSelected;
    DeckStats stats;

    void Start()
    {
        cardsSelected = new List<Card>();
        stats = GetComponent<DeckStats>();
    }

    void Update()
    {
        
    }

    public void AddCard(Card c)
    {
        if(cardsSelected.Count < maxCardAmount)
        {
            cardsSelected.Insert(0, c);
            GameObject g = selectedCardGrid.CreateCard(cardPrefab);
            g.GetComponent<CardObject>().SetCard(c, c.cost, c.num, c.type, CardObject.Context.selectedForDeckBuilding);
            cardCount.text = cardsSelected.Count + "/" + maxCardAmount;
            EditDeckStats(c, 1);
        }
        else
        {
            //Full deck feedback
        }
    }


    public void RemoveCard(Card c, GameObject g)
    {
        if(cardsSelected.Count > 0)
        {
            cardsSelected.Remove(c);
            selectedCardGrid.RemoveObj(g);
            cardCount.text = cardsSelected.Count + "/" + maxCardAmount;
            EditDeckStats(c, -1);
        }
    }

    void EditDeckStats(Card c, int num)
    {
        stats.EditCostStat(c.cost, num);
        stats.EditStyleStat(c.type, num);

        DeckBuilderManager.TypeFilter type;
        if (c is AttackCard)
            type = DeckBuilderManager.TypeFilter.attack;
        else if (c is BuffCard)
            type = DeckBuilderManager.TypeFilter.buff;
        else if (c is DefenseCard)
            type = DeckBuilderManager.TypeFilter.defense;
        else
            type = DeckBuilderManager.TypeFilter.heal;

        stats.EditTypeStat(type, num);
    }

    public List<Card> GetSelectedCards()
    {
        return cardsSelected;
    }

    public void SaveDeck()
    {
        SavedDeck.SaveDeck(this);
    }
}
