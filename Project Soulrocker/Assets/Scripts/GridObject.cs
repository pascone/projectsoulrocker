﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//[RequireComponent(typeof(Button))]
public class GridObject : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    protected bool hovered = false;
    protected bool force = false;
    protected Button button;
    EventSystem eventSystem;

    private void Start()
    {
        button = GetComponent<Button>();
        eventSystem = FindObjectOfType<EventSystem>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (button)
        {
            if (button.interactable)
                hovered = true;
        }
        else
        {
            hovered = true;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!force)
            hovered = false;

        if(eventSystem)
            eventSystem.SetSelectedGameObject(null);
    }

    public bool GetHovered()
    {
        return hovered;
    }

    public void ForceHovered()
    {
        hovered = true;
        force = true;
    }
}
