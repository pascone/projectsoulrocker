﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Card : ScriptableObject
{
    public enum Type
    {
        BASH,
        SLASH,
        THROW,
    }

    public string cardName;     //The name of the card
    public string description;  //The dexcription of the card
    public Type type;           //What type of card it is
    public int cost;            //How much strength this card will take to use
    public float num;             //Damage amount/heal amount/etc
    public Sprite sprite;       //Image use for the card
    public CardEffectEnum effectEnum;
    public EffectType effectType;

    public abstract void Activate(Player me, Player enemy, CardObject obj, float optionalNumber);
}
