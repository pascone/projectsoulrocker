﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Strength : MonoBehaviour
{
    public TextMeshProUGUI strengthUI;
        
    int currentStrength = 0;           //The current strength of this player
    Animator animator;

    private void Awake()
    {
        animator = strengthUI.transform.parent.GetComponent<Animator>();
    }

    //Takes in a number (positive or negative) and adds it to the currentStrength
    //Makes sure currentStrength doesn't go above maxStrength ot below 0
    public void EditStrength(int num)
    {
        currentStrength += num;

        if (num > 0)
            animator.SetTrigger("Increase");
        else if(num < 0)
        {
            animator.SetTrigger("Decrease");
            if (currentStrength < 0)
                currentStrength = 0;
        }

        strengthUI.text = currentStrength.ToString();
    }

    public void NotEnoughStrength()
    {
        animator.SetTrigger("NotEnough");
    }

    public int GetCurrentStrength()
    {
        return currentStrength;
    }
}
