﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EndScreen : MonoBehaviour
{
    public GameObject gameUI;
    public GameObject endUI;
    public TextMeshProUGUI winnerName;
    public TextMeshProUGUI loserName;

    public void EndGame(Player winner, Player loser)
    {
        gameUI.SetActive(false);
        winnerName.text = winner.GetCharacter().characterName;
        loserName.text = loser.GetCharacter().characterName;
        endUI.SetActive(true);
        StartCoroutine(EndVoiceLines(winner, loser));
        Time.timeScale = 1;
    }

    IEnumerator EndVoiceLines(Player winner, Player loser)
    {
        winner.GetCharacter().PlayVoiceLine(Character.VoiceLines.win);
        while(winner.GetCharacter().CheckIfVoiceLineIsPlaying())
            yield return new WaitForEndOfFrame();

        loser.GetCharacter().PlayVoiceLine(Character.VoiceLines.lose);
    }
}
