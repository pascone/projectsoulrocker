﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckBuilderCardShower : MonoBehaviour
{
    public GameObject cardPrefab;
    public GameObject cardPrefabNoLayout;
    public bool usingUnityGrid;

    CardGrid grid;

    void Awake()
    {
        grid = GetComponent<CardGrid>();
    }

    public void ShowCards(List<Card> cards)
    {
        if (usingUnityGrid)
        {
            while (transform.childCount > 0)
            {
                GameObject g = transform.GetChild(0).gameObject;
                transform.GetChild(0).SetParent(null);
                Destroy(g);
            }
        }
        else
        {
            grid.ClearGrid();
        }

        foreach (Card c in cards)
        {
            GameObject g;
            if (usingUnityGrid)
            {
                g = Instantiate(cardPrefab, transform);
                g.GetComponent<Animator>().SetTrigger("Open");
            }
            else
            {
                g = grid.CreateCard(cardPrefab);
            }

            g.GetComponent<CardObject>().SetCard(c, c.cost, c.num, c.type, CardObject.Context.showingForDeckBulding);
        }
    }
}
