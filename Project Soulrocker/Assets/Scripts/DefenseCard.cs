﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewDefenseCard", menuName = "Card/Defense Card", order = 4)]

public class DefenseCard : Card
{
    public override void Activate(Player me, Player enemy, CardObject obj, float optionalNumber)
    {
    }
}
