﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneFree : Character
{
    Player player;

    private void Awake()
    {
        player = GetComponentInParent<Player>();
    }

    public override void SpecialAbility()
    {
        SplashManager.Instance.CreateSplash("Stone Free!", Color.yellow, Color.black);
        StartCoroutine(DelayAbility());
    }

    IEnumerator DelayAbility()
    {
        yield return new WaitForSeconds(2);
        player.EditStrength(5);
    }
}
