﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Belvedere : Character
{
    Player player;

    private void Awake()
    {
        player = GetComponentInParent<Player>();
    }

    public override void SpecialAbility()
    {
        SplashManager.Instance.CreateSplash("Belvedere!", Color.yellow, Color.black);
        player.GetHand().PlayCardsInOrder();
    }
}
