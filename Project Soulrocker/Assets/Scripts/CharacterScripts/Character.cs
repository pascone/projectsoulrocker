﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{
    public enum VoiceLines
    {
        attack = 0,//
        buff,//
        charged,//
        damaged,//
        defendAttack,//
        defense,//
        heal,//
        lose,
        lowStrength,//
        pickTurn,//
        playTurn,//
        special,//
        win
    }

    [System.Serializable]
    public struct VoiceClips
    {
        public AudioClip[] lines;
    }

    public string characterName;        //Name of this character
    public string abilityDescription;       
    public Sprite[] sprites;            //Sprites of the character
    public VoiceClips[] voiceClips;      //Voice clips of the character
    AudioSource audioSource;
    int[] lastPlayed;

    public abstract void SpecialAbility();

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        lastPlayed = new int[voiceClips.Length];
        for(int i = 0; i < lastPlayed.Length; i++)
        {
            lastPlayed[i] = -1;
        }
    }

    public void PlayVoiceLine(VoiceLines line)
    {
        AudioClip[] allLinesOfType = voiceClips[(int)line].lines;
        int index = Random.Range(0, allLinesOfType.Length);
        if (voiceClips[(int)line].lines.Length > 1)
        {
            while (index == lastPlayed[(int)line])
            {
                index = Random.Range(0, allLinesOfType.Length);
            }
        }
        AudioClip lineToPlay = allLinesOfType[index];
        if(!audioSource.isPlaying)
            audioSource.PlayOneShot(lineToPlay);

        lastPlayed[(int)line] = index;
    }

    public bool CheckIfVoiceLineIsPlaying()
    {
        return audioSource.isPlaying;
    }
}
