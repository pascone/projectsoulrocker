﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WildFire : Character
{
    int playerNum;

    private void Awake()
    {
        playerNum = GetComponentInParent<Player>().playerNum;
    }

    public override void SpecialAbility()
    {
        SplashManager.Instance.CreateSplash("Wildfire!", Color.yellow, Color.black);
        CardGameManager.Instance.WildFirePlayed(playerNum);
    }

}
