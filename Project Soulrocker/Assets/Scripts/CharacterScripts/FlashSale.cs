﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashSale : Character
{
    CardGrid middleHand;

    private void Awake()
    {
        middleHand = GameObject.Find("MiddleHand").GetComponent<CardGrid>();
    }

    public override void SpecialAbility()
    {
        SplashManager.Instance.CreateSplash("Flash Sale!", Color.yellow, Color.black);
        StartCoroutine(DelayAbility());
    }

    IEnumerator DelayAbility()
    {
        yield return new WaitForSeconds(2);
        for (int i = 0; i < middleHand.transform.childCount; i++)
        {
            middleHand.transform.GetChild(i).GetComponent<CardObject>().EditCost(1);
        }
    }
}
