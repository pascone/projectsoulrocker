﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DeckBuilderManager : MonoBehaviour
{
    [System.Serializable]
    public enum CostFilter
    {
        zeroCost,
        oneCost,
        twoCost,
        threeCost,
        fourOrMoreCost,
        maxFilterSize
    }

    [System.Serializable]
    public enum TypeFilter
    {
        attack,
        buff,
        defense,
        heal,
        maxFilterSize
    }

    [System.Serializable]
    public enum StyleFilter
    {
        smash,
        slash,
        shoot,
        maxFilterSize
    }

    public DeckBuilderCardShower mainCardShower;
    public TextMeshProUGUI cardCount;
    public Card[] allCards;

    bool[] costFilterStatus;
    bool[] typeFilterStatus;
    bool[] styleFilterStatus;

    void Start()
    {
        costFilterStatus = new bool[(int)CostFilter.maxFilterSize];
        typeFilterStatus = new bool[(int)TypeFilter.maxFilterSize];
        styleFilterStatus = new bool[(int)StyleFilter.maxFilterSize];

        for (int i = 0; i < costFilterStatus.Length; i++)
            costFilterStatus[i] = true;

        for (int i = 0; i < typeFilterStatus.Length; i++)
            typeFilterStatus[i] = true;

        for (int i = 0; i < styleFilterStatus.Length; i++)
            styleFilterStatus[i] = true;

        UpdateShownCards();
    }

    void Update()
    {
        
    }

    public void EditCostFilter(int cost)
    {
        costFilterStatus[cost] = !costFilterStatus[cost];

        UpdateShownCards();
    }

    public void EditTypeFilter(int type)
    {
        typeFilterStatus[type] = !typeFilterStatus[type];

        UpdateShownCards();
    }

    public void EditStyleFilter(int style)
    {
        styleFilterStatus[style] = !styleFilterStatus[style];

        UpdateShownCards();
    }

    public void UpdateShownCards()
    {
        List<Card> cardsToShow = new List<Card>();
        
        foreach(Card c in allCards)
        {
            if (CheckCard(c))
                cardsToShow.Add(c);
        }

        mainCardShower.ShowCards(cardsToShow);
        cardCount.text = "Currently showing " + cardsToShow.Count + " out of " + allCards.Length + " cards";
    }

    bool CheckCard(Card c)
    {
        if (c.cost >= 4 && !costFilterStatus[4])
            return false;
        else
            if (!costFilterStatus[c.cost])
                return false;

        switch(c.type)
        {
            case Card.Type.THROW:
                if (!styleFilterStatus[2])
                    return false;
                break;
            case Card.Type.SLASH:
                if (!styleFilterStatus[1])
                    return false;
                break;
            case Card.Type.BASH:
                if (!styleFilterStatus[0])
                    return false;
                break;
        }

        if (c is AttackCard && !typeFilterStatus[0])
            return false;
        else if (c is BuffCard && !typeFilterStatus[1])
            return false;
        else if (c is DefenseCard && !typeFilterStatus[2])
            return false;
        else if (c is HealCard && !typeFilterStatus[3])
            return false;

        return true;
    }
}
