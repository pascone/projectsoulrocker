﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int playerNum;       //The players number (0-3). Used for various managment functions
    public Animator imageAnim;
    public GameObject[] allCharacters;

    Hand hand;                  //The players hand of cards
    Health health;              //The players health
    Strength strength;          //The players strength
    Character character;        //The players chosen character
    SpecialAbility specialAbility;        //The players special ability meter

    BoostObject dontDestroyDefensesObject;
    List<BoostObject> enemyBoostsICanUse = new List<BoostObject>();

    bool damageEnemyBasedOnNextHit = false;

    private void Awake()
    {
        hand = GetComponent<Hand>();
        health = GetComponent<Health>();
        strength = GetComponent<Strength>();
        specialAbility = GetComponent<SpecialAbility>();

        //TEMP
        int characterChosen = CharacterChooser.GetCharacterChosen();
        if (playerNum == 1)
        {
            int i = characterChosen;
            while (i == characterChosen)
            {
                i = Random.Range(0, 4);
            }
            characterChosen = i;
        }
        ChooseCharacter(characterChosen);

        specialAbility.Init(character.characterName, character.abilityDescription);
    }

    private void Update()
    {
        //TEMP
        if (Input.GetKeyDown(KeyCode.Space) && specialAbility.CheckSpecialAbility() && playerNum == 0 &&
            CardGameManager.Instance.GetPlayerTurn() == playerNum && CardGameManager.Instance.GetPhase() == CardGameManager.Phase.pickCards)
        {
            UseSpecialAbility();
        }

        hand.CheckCardStrength(strength.GetCurrentStrength());
    }

    //Takes in a positive number for healing or a negative number for damaging and edits the health with it
    public void EditHealth(int num, bool heal)
    {
        if (heal)
        {
            if (CardEffectManager.Instance.CheckActiveEffect(CardEffectEnum.bubbleWrap, playerNum))
                num = 0;

            if (CardEffectManager.Instance.CheckActiveEffect(CardEffectEnum.bananaPeel, playerNum))
            {
                num *= -1;
                SplashManager.Instance.DamageSplash(playerNum, num);
            }
            else
                SplashManager.Instance.HealSplash(playerNum, num);
        }
        else
        {
            SplashManager.Instance.DamageSplash(playerNum, num);
            if(damageEnemyBasedOnNextHit)
            {
                CardGameManager.Instance.players[(playerNum - 1) * -1].EditHealth(num, false);
                damageEnemyBasedOnNextHit = false;
            }
        }

        //EditHealth returns true if they are dead
        if (health.EditHealth(num))
        {
            StartCoroutine(Dead());
        }
        else
        {
            if (num < 0)
                character.PlayVoiceLine(Character.VoiceLines.damaged);
        }
    }

    IEnumerator Dead()
    {
        Time.timeScale = 0.5f;
        yield return new WaitForSecondsRealtime(2);
        CardGameManager.Instance.EndGame(playerNum);
    }

    public void EditStrength(int num)
    {
        strength.EditStrength(num);
    }

    public void NotEnoughStrength()
    {
        strength.NotEnoughStrength();
    }

    public int GetStrength()
    {
        return strength.GetCurrentStrength();
    }

    public void AddEnemyBuffICanUse(BoostObject b)
    {
        enemyBoostsICanUse.Add(b);
    }

    public float CheckBuffs(Card.Type type)
    {
        float multiplier = 1;
        for (int i = 0; i < hand.boostGrid.transform.childCount; i++)
        {
            BoostObject b = hand.boostGrid.transform.GetChild(i).GetComponent<BoostObject>();
            if (b)
            {
                if (b.CheckIfBoost() && b.type.text == type.ToString().ToUpper())
                {
                    multiplier *= b.GetMultiplier();
                    b.Use();
                }
            }

        }

        foreach(BoostObject b in enemyBoostsICanUse)
        {
            if (b)
            {
                if (b.type.text == type.ToString().ToUpper())
                {
                    multiplier *= b.GetMultiplier();
                    b.Use();
                }
            }
        }
        return multiplier;
    }

    public float DestroyAllBuffs(BoostObject dontDestroyThisBoost = null)
    {
        float multiplier = 1;
        for (int i = 0; i < hand.boostGrid.transform.childCount; i++)
        {
            BoostObject b = hand.boostGrid.transform.GetChild(i).GetComponent<BoostObject>();
            if (b)
            {
                if (b.CheckIfBoost())
                {
                    if (b != dontDestroyThisBoost)
                    {
                        multiplier *= b.GetMultiplier();
                        b.Use();
                    }
                }
            }
        }
        foreach (BoostObject b in enemyBoostsICanUse)
        {
            if (b)
            {
                multiplier *= b.GetMultiplier();
                b.Use();
            }
        }
        return multiplier;
    }

    public float DestroyAllDefenses()
    {
        float divider = 1;
        for (int i = 0; i < hand.boostGrid.transform.childCount; i++)
        {
            BoostObject b = hand.boostGrid.transform.GetChild(i).GetComponent<BoostObject>();
            if (b)
            {
                if (!b.CheckIfBoost())
                {
                    divider /= b.GetMultiplier();
                    b.Use();
                }
            }
        }
        return divider;
    }

    public float CheckDefense(Card.Type type)
    {
        float divider = 1;
        bool flag = false;

        for (int i = 0; i < hand.boostGrid.transform.childCount; i++)
        {
            BoostObject b = hand.boostGrid.transform.GetChild(i).GetComponent<BoostObject>();
            if (b)
            {
                if (!b.CheckIfBoost() && b.type.text == type.ToString().ToUpper())
                {
                    divider /= b.GetMultiplier();
                    if (CardEffectManager.Instance.CheckActiveEffect(CardEffectEnum.glue, playerNum))
                    {
                        b.UseDontDestroy();
                        flag = true;
                    }
                    else
                        b.Use();
                }
            }

        }

        if (flag)
            dontDestroyDefensesObject.UseDontDestroy();

        return divider;
    }

    public void ChangeAllBoostStyle(Card.Type newType)
    {
        for (int i = 0; i < hand.boostGrid.transform.childCount; i++)
        {
            BoostObject b = hand.boostGrid.transform.GetChild(i).GetComponent<BoostObject>();
            if (b)
            {
                b.EditType(newType);
            }
        }
    }

    //Checks if health is at 0
    public bool CheckDeath()
    {
        return false;
    }

    public Hand GetHand()
    {
        return hand;
    }

    public SpecialAbility GetSpecialAbility()
    {
        return specialAbility;
    }

    public void ExpandCharacterImage()
    {
        imageAnim.ResetTrigger("Retract");
        imageAnim.SetTrigger("Expand");
    }

    public void RetractCharacterImage()
    {
        imageAnim.ResetTrigger("Expand");
        imageAnim.SetTrigger("Retract");
    }

    void ChooseCharacter(int characterIndex)
    {
        GameObject g = Instantiate(allCharacters[characterIndex], transform);
        character = g.GetComponent<Character>();
    }

    public Character GetCharacter()
    {
        return character;
    }

    public void UseSpecialAbility()
    {
        character.SpecialAbility();
        specialAbility.UseSpecialAbility();
        character.PlayVoiceLine(Character.VoiceLines.special);
    }

    public void DontDestroyDefenses(BoostObject objectThatCausesThis)
    {
        dontDestroyDefensesObject = objectThatCausesThis;
    }

    public void EditMaxHealth(int num)
    {
        health.SetIncreaseMaxHealth(num);
    }

    public int GetMaxHealth()
    {
        return health.GetMaxHealth();
    }

    public int GetCurrentHealth()
    {
        return health.GetCurrentHealth();
    }

    public void SetHealth(int newHealth)
    {
        health.SetHealth(newHealth);
    }

    public void DamageEnemyBasedOnNextHit()
    {
        damageEnemyBasedOnNextHit = true;
    }
}
