﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeckStats : MonoBehaviour
{
    public Slider[] costSliders;
    public Slider[] styleSliders;
    public Slider[] typeSliders;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void EditCostStat(int cost, int num)
    {
        costSliders[(int)cost].value += num;
    }

    public void EditStyleStat(Card.Type style, int num)
    {
        styleSliders[(int)style].value += num;
    }

    public void EditTypeStat(DeckBuilderManager.TypeFilter type, int num)
    {
        typeSliders[(int)type].value += num;
    }
}
