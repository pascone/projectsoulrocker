﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorManager : MonoBehaviour
{
    #region SINGLETON PATTERN
    public static ColorManager _instance;
    public static ColorManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ColorManager>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("ColorManager");
                    _instance = container.AddComponent<ColorManager>();
                }
            }

            return _instance;
        }
    }
    #endregion

    public Color smashColor;
    public Color slashColor;
    public Color shootColor;
    public Color attackColor;
    public Color healColor;
    public Color defenseColor;
    public Color buffColor;

    public Sprite buffSprite;
    public Sprite attackSprite;
    public Sprite defenseSprite;
    public Sprite healSprite;
}
