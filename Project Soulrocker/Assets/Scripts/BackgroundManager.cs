﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BackgroundManager : MonoBehaviour
{
    #region SINGLETON PATTERN
    public static BackgroundManager _instance;
    public static BackgroundManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<BackgroundManager>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("BackgroundManager");
                    _instance = container.AddComponent<BackgroundManager>();
                }
            }

            return _instance;
        }
    }
    #endregion

    public Image background;
    public Animator animator;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void MoveBackgroundLeft()
    {
        animator.SetTrigger("Left");
    }

    public void MoveBackgroundRight()
    {
        animator.SetTrigger("Right");
    }

    public void MoveBackgroundMiddle()
    {
        animator.SetTrigger("Middle");
    }
}
