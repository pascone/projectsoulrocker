﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Filter : GridObject
{
    public string[] options;

    Toggle[] optionObjs;
    Transform optionHolder;

    void Start()
    {
        optionObjs = new Toggle[5];

        optionHolder = transform.GetChild(1);
        for(int i = 0; i < optionHolder.childCount; i++)
        {
            optionObjs[i] = optionHolder.GetChild(i).GetComponent<Toggle>();

            if(i >= options.Length)
            {
                optionObjs[i].gameObject.SetActive(false);
            }
            else if (i < options.Length)
            {
                optionObjs[i].transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = options[i];
            }
        }

        HideOptions();
    }

    void Update()
    {
        if(!hovered)
        {
            HideOptions();
        }
    }

    public void ShowOptions()
    {
        if (optionHolder.gameObject.activeSelf)
        {
            HideOptions();
        }
        else
        {
            optionHolder.gameObject.SetActive(true);
        }
    }

    public void HideOptions()
    {
        optionHolder.gameObject.SetActive(false);
    }
}
