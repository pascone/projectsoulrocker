﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class SavedDeck
{
    static Card[] savedDeck;

    public static void SaveDeck(SelectedCardsForDeck selectManager)
    {
        List<Card> cardsToSave = selectManager.GetSelectedCards();

        if (cardsToSave.Count == selectManager.maxCardAmount)
        {
            savedDeck = new Card[cardsToSave.Count];
            for (int i = 0; i < savedDeck.Length; i++)
            {
                savedDeck[i] = cardsToSave[i];
            }

            SceneManager.LoadScene(0);
        }
    }

    public static Card[] GetSavedDeck()
    {
        return savedDeck;
    }
}
