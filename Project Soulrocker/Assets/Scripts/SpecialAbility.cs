﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SpecialAbility : MonoBehaviour
{
    public Color readyColor;
    public Color notReadyColor;
    public Slider abilitySlider;
    public Image fill;
    public TextMeshProUGUI percentageUI;
    public TextMeshProUGUI characterNameUI;
    public SpecialAbilityObject abilityObject;

    int abilityCharge = 0;
    string characterName;
    string abilityDescription;
    bool initialized = false;

    void Start()
    {
        fill.color = notReadyColor;
    }

    void Update()
    {
        if (initialized)
        {
            if (abilityObject.GetHovered())
            {
                characterNameUI.text = "Special Ability: " + abilityDescription;
            }
            else
            {
                characterNameUI.text = characterName;
            }
        }
    }

    public void Init(string _name, string _desc)
    {
        initialized = true;
        characterName = _name;
        abilityDescription = _desc;
    }

    public bool CheckSpecialAbility()
    {
        return abilityCharge >= 100;
    }

    public void EditSpecialAbilityBasedOnDamage(int damage, bool wasTaken)
    {
        if (wasTaken)
            damage /= 2;

        abilityCharge += damage;

        if (abilityCharge >= 100)
        {
            fill.color = readyColor;
            abilityCharge = 100;
            abilitySlider.GetComponent<Animator>().SetBool("Charged", true);
        }

        abilitySlider.value = abilityCharge;
        percentageUI.text = abilityCharge + "%";
    }

    public void EditSpecialAbilityBasedOnCardUse()
    {
        abilityCharge += 7;

        if (abilityCharge >= 100)
        {
            fill.color = readyColor;
            abilityCharge = 100;
            abilitySlider.GetComponent<Animator>().SetBool("Charged", true);
        }

        abilitySlider.value = abilityCharge;
        percentageUI.text = abilityCharge + "%";
    }

    public void UseSpecialAbility()
    {
        abilityCharge = 0;
        abilitySlider.value = abilityCharge;
        percentageUI.text = abilityCharge + "%";
        abilitySlider.GetComponent<Animator>().SetBool("Charged", false);
        fill.color = notReadyColor;
    }
}
