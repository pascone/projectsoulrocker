﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct CardInfo
{
    public Card card;
    public GameObject cardObj;
}

public struct BoostInfo
{
    public Card card;
    public GameObject obj;
}

public class Hand : MonoBehaviour
{
    public CardGrid cardGrid;
    public CardGrid boostGrid;
    public GameObject cardPrefab;

    List<CardInfo> cards;       //List of all the cards currently in this hand    
    bool playCardsInOrder = false;

    List<int> energyDrinkInHand = new List<int>();

    private void Start()
    {
        cards = new List<CardInfo>();
    }

    //Takes a card and adds it to the cards list
    public CardObject AddToHand(Card card, int editedCost, float editedNum, Card.Type editedType)
    {
        GameObject g = cardGrid.CreateCard(cardPrefab);
        CardObject c = g.GetComponent<CardObject>();
        c.SetCard(card, editedCost, editedNum, editedType);

        CardInfo cardInfo = new CardInfo();
        cardInfo.card = card;
        cardInfo.cardObj = g;

        cards.Insert(0, cardInfo);

        return c;
    }

    //Chooses a random card from the cards list, removes it from that list, and returns it
    public CardInfo UseRandomCard(bool overrideCardsInOrder = false)
    {
        CardInfo c = new CardInfo();

        if (cards.Count == 0)
        {
            c.card = null;
            c.cardObj = null;
            return c;
        }

        int num = Random.Range(0, cards.Count);
        if (playCardsInOrder && !overrideCardsInOrder)
            num = cards.Count - 1;
        else if (energyDrinkInHand.Count > 0)
        {
            num = energyDrinkInHand[0];
            energyDrinkInHand.RemoveAt(0);
        }

        c = cards[num];
        cards.RemoveAt(num);

        if (cards.Count == 0)
            playCardsInOrder = false;

        return c;
    }

    public void PlayCardsInOrder()
    {
        playCardsInOrder = true;
    }

    public void CheckCardStrength(int currStrength)
    {
        List<GameObject> objs = cardGrid.GetObjects();

        foreach(GameObject card in objs)
        {
            card.GetComponent<CardObject>().CheckCost(currStrength);
        }
    }

    public bool CheckIfYouHaveOneOfEveryStyleInYourHand()
    {
        bool slash = false;
        bool smash = false;
        bool shoot = false;

        foreach(CardInfo c in cards)
        {
            if (c.card.type == Card.Type.THROW)
                shoot = true;
            else if (c.card.type == Card.Type.SLASH)
                slash = true;
            else if (c.card.type == Card.Type.BASH)
                smash = true;
        }

        return slash && smash && shoot;
    }

    public bool IsHandEmpty()
    {
        return cardGrid.transform.childCount == 0;
    }

    public void SetEnergyDrink(int energyDrinkIndex)
    {
        energyDrinkInHand.Add(energyDrinkIndex);
    }
}
