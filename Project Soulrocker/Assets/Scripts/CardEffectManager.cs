﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CardEffectEnum
{
    none = -1,
    baseballBat,
    stealPipe,
    fryingPan,
    golfClub,
    shovel,
    pizzaCutter,
    scissors,
    sickle,
    saw,
    axe,
    dart,
    boomerang,
    spikedBaseball,
    glue,
    ductTape,
    bubbleWrap,
    bananaPeel,
    energyDrink,
    spinach,
    livestream,
    disinfectant,
    lewdMagazine,
    incense,
    boombox,
    headbutt,
    mouseTrap,
    lunchBox,
    mascotCostume,
    surpriseTrampoline,
    baseballGlove,
    pencil,
    glassBeaker,
    bagOfMarbles,
    woodenPlank,
    bicycle,
    MAX
}

public enum EffectType
{
    none,
    onPlay,
    onUse,
    passive
}

public class CardEffectManager : MonoBehaviour
{
    #region SINGLETON PATTERN
    public static CardEffectManager _instance;
    private static bool applicationIsQuitting = false;

    public static CardEffectManager Instance
    {
        get
        {
            if (applicationIsQuitting)
                return null;

            if (_instance == null)
            {
                _instance = FindObjectOfType<CardEffectManager>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("CardEffectManager");
                    _instance = container.AddComponent<CardEffectManager>();
                }
            }

            return _instance;
        }
    }

    public void OnDestroy()
    {
        applicationIsQuitting = true;
    }
    #endregion

    CardEffect[] cardEffects;
    List<int>[] activeEffects = new List<int>[(int)CardEffectEnum.MAX]; //Stores the playerNum that the effect is for. -1 is for all players

    private void Awake()
    {
        applicationIsQuitting = false;
        cardEffects = new CardEffect[(int)CardEffectEnum.MAX];

        cardEffects[0] = new BaseballBatEffect();
        cardEffects[1] = new StealPipeEffect();
        cardEffects[2] = new FryingPanEffect();
        cardEffects[3] = new GolfClubEffect();
        cardEffects[4] = new ShovelEffect();
        cardEffects[5] = new PizzaCutterEffect();
        cardEffects[6] = new ScissorsEffect();
        cardEffects[7] = new SickleEffect();
        cardEffects[8] = new SawEffect();
        cardEffects[9] = new AxeEffect();
        cardEffects[10] = new DartEffect();
        cardEffects[11] = new BoomerangEffect();
        cardEffects[12] = new SpikedBaseballEffect();
        cardEffects[13] = new GlueEffect();
        cardEffects[14] = new DuctTapeEffect();
        cardEffects[15] = new BubbleWrapEffect();
        cardEffects[16] = new BananaPeelEffect();
        cardEffects[17] = new EnergyDrinkEffect();
        cardEffects[18] = new SpinachEffect();
        cardEffects[19] = new LivestreamEffect();
        cardEffects[20] = new DisinfectantEffect();
        cardEffects[21] = new LewdMagazineEffect();
        cardEffects[22] = new IncenseEffect();
        cardEffects[23] = new BoomboxEffect();
        cardEffects[24] = new HeadbuttEffect();
        cardEffects[25] = new MouseTrapEffect();
        cardEffects[26] = new LunchBoxEffect();
        cardEffects[27] = new MascotCostumeEffect();
        cardEffects[28] = new SurpriseTrampolineEffect();
        cardEffects[29] = new BaseballGloveEffect();
        cardEffects[30] = new PencilEffect();
        cardEffects[31] = new GlassBeakerEffect();
        cardEffects[32] = new BagOfMarblesEffect();
        cardEffects[33] = new WoodenPlankEffect();
        cardEffects[34] = new BicycleEffect();

        for(int i = 0; i < (int)CardEffectEnum.MAX; i++)
        {
            activeEffects[i] = new List<int>();
        }
    }

    public CardEffect GetCardEffect(CardEffectEnum effectEnum)
    {
        if (effectEnum == CardEffectEnum.none)
            return null;
        return cardEffects[(int)effectEnum];
    }

    public void ActivateEffect(CardEffectEnum effectEnum, int playerNum)
    {
        activeEffects[(int)effectEnum].Add(playerNum);
    }

    public bool CheckActiveEffect(CardEffectEnum effectEnum, int playerNum)
    {
        foreach(int i in activeEffects[(int)effectEnum])
        {
            if (i == playerNum || i == -1)
                return true;
        }
        return false;
    }

    public void RemoveEffect(CardEffectEnum effectEnum, int playerNum)
    {
        activeEffects[(int)effectEnum].Remove(playerNum);
    }
}
