﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class CharacterChooser
{
    static int characterChosen;

    public static void ChooseCharacter(int characterNum)
    {
        characterChosen = characterNum;
        SceneManager.LoadScene(1);
    }

    public static int GetCharacterChosen()
    {
        return characterChosen;
    }
}
