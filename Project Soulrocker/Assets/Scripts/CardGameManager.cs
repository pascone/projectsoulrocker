﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CardGameManager : MonoBehaviour
{
    #region SINGLETON PATTERN
    public static CardGameManager _instance;
    public static CardGameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<CardGameManager>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("CardGameManager");
                    _instance = container.AddComponent<CardGameManager>();
                }
            }

            return _instance;
        }
    }
    #endregion

    public enum Phase
    {
        pickCards,
        playCards,
    }

    public int cardsToDraw = 10;    //The number of cards that should be drawn in the pick cards phase
    public Player[] players;        //All the players in this game
    public Animator[] playerUI;     //All the players in this game
    public CardGrid middleHand;     //The empty gameobject that will be used to hold the cards for the pick cards phase   
    public GameObject cardPrefab;   //The prefab for the card to create when drawing the cards
    public GameObject defensePrefab;//The prefab for the defense to create when a defense card is played
    public GameObject buffPrefab;   //The prefab for the buff to create when a buff card is played

    int playerTurn;                 //The player number (0-3) of whose turn it is
    //SceneManager sm;              //SceneManager reference
    //AudioManager am;              //AudioManager reference
    Deck deck;                      //Reference to the deck
    EndScreen endScreen;
    Phase phase;                    //Stores the current phase of the card game
    int wildFire = -1;

    //Card effect specific variables
    bool ignoreBuffs = false;
    bool ignoreDefenses = false;
    bool checkEnemyBuffs = false;
    float[] damageForEndOfRound = new float[2];
    float[] healForEndOfRound = new float[2];
    public Card baseballCard;
    bool swapPlayersHealth = false;
    bool checkAllDefenseandBuffs = false;

    private void Start()
    {
        deck = GetComponent<Deck>();
        endScreen = GetComponent<EndScreen>();

        //This may have to change later. Hard coding card draw to start right away.
        playerTurn = 0;
        DrawCardsForRound();
        players[0].EditStrength(1);
        players[1].EditStrength(1);
    }

    //Will switch the playerTurn variable to the correct next player turn
    void SwitchTurn()
    {
        //This works for any number of players, but it'll always be the same rotation
        playerTurn++;
        if (playerTurn >= players.Length)
            playerTurn = 0;

        //TEMP-----------------------------------------------------------
        if(phase == Phase.pickCards)
        {
            DeactivateCardsForAITurn();
        }
        //TEMP---------------------------------------------------------------
    }

    //TEMP---------------------------------------------------------------
    void DeactivateCardsForAITurn()
    {
        if (playerTurn == 1)
        {
            Button[] butt = FindObjectsOfType<Button>();
            foreach (Button b in butt)
            {
                if (b.transform.parent == middleHand.transform)
                {
                    b.interactable = false;
                }
            }
            StartCoroutine(AIPick());
        }
        else
        {
            Button[] butt = FindObjectsOfType<Button>();
            foreach (Button b in butt)
            {
                b.interactable = true;
            }
        }
    }

    IEnumerator AIPick()
    {
        yield return new WaitForSeconds(1.5f);
        if (players[1].GetSpecialAbility().CheckSpecialAbility())
        {
            players[1].UseSpecialAbility();
            yield return new WaitForSeconds(2);
        }
        int num = Random.Range(0, middleHand.transform.childCount);
        if(middleHand.transform.childCount > 0)
            middleHand.transform.GetChild(num).GetComponent<CardObject>().ClickCard();
    }
    //TEMP---------------------------------------------------------------

    //Will draw all the needed cards from the deck to the middle
    void DrawCardsForRound()
    {
        //Set the phase
        phase = Phase.pickCards;

        //Move booster grids over
        foreach(Player p in players)
        {
            p.GetHand().boostGrid.GetComponent<Animator>().SetTrigger("MoveToSide");
        }

        foreach (Animator a in playerUI)
        {
            a.SetTrigger("Middle");
        }

        StartCoroutine(DrawCards());
    }

    //Draws all the cards with a buffer of time between each
    IEnumerator DrawCards()
    {
        List<Button> cardButtons = new List<Button>();
        for (int i = 0; i < cardsToDraw; i++)
        {
            yield return new WaitForSeconds(0.5f);
            GameObject g = middleHand.CreateCard(cardPrefab);
            Card c = deck.DrawCard();
            g.GetComponent<CardObject>().SetCard(c, c.cost, c.num, c.type);
            g.GetComponent<Button>().interactable = false;
            cardButtons.Add(g.GetComponent<Button>());
        }

        SplashManager.Instance.CreateSplash("CHOOSE YOUR WEAPONS", Color.blue, Color.white);

        yield return new WaitForSeconds(2);

        //TEMP----------------------------------
        DeactivateCardsForAITurn();
        //foreach (Button b in cardButtons)
        //{
        //    b.interactable = true;
        //}
        //TEMP----------------------------------

        ExpandAndRetractPlayers();
    }

    //Gets the hand from the player whose turn it is and uses a random card from it
    void PlayCardPhase()
    {
        //Set the phase
        phase = Phase.playCards;

        SplashManager.Instance.CreateSplash("LET YOUR ANGER TAKE CONTROL", Color.red, Color.white);

        //Move booster grids back to middle
        foreach (Player p in players)
        {
            p.GetHand().boostGrid.GetComponent<Animator>().SetTrigger("MoveToMiddle");
        }

        //Switch the turn to the next player
        SwitchTurn();
        StartCoroutine(MovePlayerUIForTurn());

        StartCoroutine(ActivateCards());
    }

    IEnumerator ActivateCards()
    {
        yield return new WaitForSeconds(2);
        //for(int i = 0; i < 5 * players.Length; i++)
        while(!players[0].GetHand().IsHandEmpty() || !players[1].GetHand().IsHandEmpty())
        {
            CardInfo cardToUse = players[playerTurn].GetHand().UseRandomCard();
            if (cardToUse.card == null)
            {
                //Play no cards voice line
                players[playerTurn].EditStrength(1);
            }
            else
            {
                yield return new WaitForSeconds(1);
                players[playerTurn].GetCharacter().PlayVoiceLine(Character.VoiceLines.playTurn);


                players[playerTurn].EditStrength(1);

                //Show the chosen card through an animation
                cardToUse.cardObj.GetComponent<Animator>().SetTrigger("Activate");
                cardToUse.cardObj.GetComponent<CardObject>().ForceHovered();

                int otherPlayer = (playerTurn - 1) * -1;

                CardObject cardToUseObject = cardToUse.cardObj.GetComponent<CardObject>();
                bool playCardTwice = false;

                if(CardEffectManager.Instance.CheckActiveEffect(CardEffectEnum.glassBeaker, playerTurn))
                {
                    cardToUseObject.EditType(Card.Type.THROW);
                    CardEffectManager.Instance.RemoveEffect(CardEffectEnum.glassBeaker, playerTurn);
                }

                if(CardEffectManager.Instance.CheckActiveEffect(CardEffectEnum.woodenPlank, playerTurn))
                {
                    if (cardToUseObject.GetCardType() == Card.Type.BASH)
                        players[playerTurn].EditStrength(2);
                    CardEffectManager.Instance.RemoveEffect(CardEffectEnum.woodenPlank, playerTurn);
                }

                yield return new WaitForSeconds(2);

                //Check if you have enough strength
                if (players[playerTurn].GetStrength() >= cardToUseObject.GetCost())
                {
                    float optionalNum = 0;
                    BoostObject boostCreated = null;
                    if (cardToUse.card is BuffCard)
                    {
                        players[playerTurn].GetCharacter().PlayVoiceLine(Character.VoiceLines.buff);
                        boostCreated = CreateNewBuff(cardToUseObject, players[playerTurn]);
                        if(CardEffectManager.Instance.CheckActiveEffect(CardEffectEnum.mascotCostume, players[playerTurn].playerNum))
                        {
                            boostCreated.EditMultiplier(1.5f, Color.yellow);
                        }
                    }
                    else if (cardToUse.card is DefenseCard)
                    {
                        players[playerTurn].GetCharacter().PlayVoiceLine(Character.VoiceLines.defense);
                        boostCreated = CreateNewDefense(cardToUseObject, players[playerTurn]);
                    }

                    if(CardEffectManager.Instance.CheckActiveEffect(CardEffectEnum.pencil, playerTurn))
                    {
                        if (cardToUseObject.GetCardType() == Card.Type.BASH)
                            playCardTwice = true;

                        CardEffectManager.Instance.RemoveEffect(CardEffectEnum.pencil, playerTurn);
                    }

                    //Activate the card effect
                    CardEffect effect = cardToUseObject.GetEffect();
                    if (effect != null)
                        effect.Activate(players[playerTurn], players[otherPlayer], cardToUseObject, boostCreated);

                    if (cardToUse.card is AttackCard)
                    {
                        AttackCard a = (AttackCard)cardToUse.card;

                        if (CardEffectManager.Instance.CheckActiveEffect(CardEffectEnum.boomerang, players[playerTurn].playerNum))
                        {
                            damageForEndOfRound[playerTurn] += cardToUse.card.num / 2;
                        }

                        float multiplier = 1;
                        if (!ignoreBuffs)
                        {
                            if(checkAllDefenseandBuffs)
                                multiplier = players[playerTurn].DestroyAllBuffs();
                            else
                                multiplier = players[playerTurn].CheckBuffs(cardToUseObject.GetCardType());
                            if (checkEnemyBuffs)
                                multiplier *= players[otherPlayer].CheckBuffs(cardToUseObject.GetCardType());
                        }

                        float divider = 1;
                        if (!ignoreDefenses)
                        {
                            if (checkAllDefenseandBuffs)
                                divider = players[otherPlayer].DestroyAllDefenses();
                            else
                                divider = players[otherPlayer].CheckDefense(cardToUseObject.GetCardType());
                        }

                        optionalNum = multiplier * divider;
                        ignoreDefenses = false;
                        ignoreBuffs = false;
                        checkEnemyBuffs = false;
                        checkAllDefenseandBuffs = false;

                        if (multiplier != 1 && divider != 1)
                        {
                            if (divider < 1)
                            {
                                players[otherPlayer].GetCharacter().PlayVoiceLine(Character.VoiceLines.defendAttack);
                            }

                            yield return new WaitForSeconds(1);
                        }
                        players[playerTurn].GetCharacter().PlayVoiceLine(Character.VoiceLines.attack);

                    }
                    else if (cardToUse.card is HealCard)
                    {
                        HealCard h = (HealCard)cardToUse.card;

                        if (CardEffectManager.Instance.CheckActiveEffect(CardEffectEnum.boombox, players[playerTurn].playerNum))
                        {
                            healForEndOfRound[playerTurn] += cardToUse.card.num / 2;
                        }

                        optionalNum = players[playerTurn].CheckBuffs(cardToUseObject.GetCardType());
                        if (optionalNum != 1)
                            yield return new WaitForSeconds(1);

                        players[playerTurn].GetCharacter().PlayVoiceLine(Character.VoiceLines.heal);
                    }

                    //Activate card
                    cardToUse.card.Activate(players[playerTurn], players[otherPlayer], cardToUseObject, optionalNum);
                    players[playerTurn].EditStrength(-cardToUseObject.GetCost());
                    players[playerTurn].GetSpecialAbility().EditSpecialAbilityBasedOnCardUse();

                    //this is gross
                    if(playCardTwice)
                    {
                        if (cardToUse.card is BuffCard)
                        {
                            players[playerTurn].GetCharacter().PlayVoiceLine(Character.VoiceLines.buff);
                            boostCreated = CreateNewBuff(cardToUseObject, players[playerTurn]);
                            if (CardEffectManager.Instance.CheckActiveEffect(CardEffectEnum.mascotCostume, players[playerTurn].playerNum))
                            {
                                boostCreated.EditMultiplier(1.5f, Color.yellow);
                            }
                        }
                        else if (cardToUse.card is DefenseCard)
                        {
                            players[playerTurn].GetCharacter().PlayVoiceLine(Character.VoiceLines.defense);
                            boostCreated = CreateNewDefense(cardToUseObject, players[playerTurn]);
                        }

                        if (CardEffectManager.Instance.CheckActiveEffect(CardEffectEnum.pencil, playerTurn))
                        {
                            if (cardToUseObject.GetCardType() == Card.Type.BASH)
                                playCardTwice = true;

                            CardEffectManager.Instance.RemoveEffect(CardEffectEnum.pencil, playerTurn);
                        }

                        //Activate the card effect
                        effect = cardToUseObject.GetEffect();
                        if (effect != null)
                            effect.Activate(players[playerTurn], players[otherPlayer], cardToUseObject, boostCreated);

                        if (cardToUse.card is AttackCard)
                        {
                            AttackCard a = (AttackCard)cardToUse.card;

                            if (CardEffectManager.Instance.CheckActiveEffect(CardEffectEnum.boomerang, players[playerTurn].playerNum))
                            {
                                damageForEndOfRound[playerTurn] += cardToUse.card.num / 2;
                            }

                            float multiplier = 1;
                            if (!ignoreBuffs)
                            {
                                if (checkAllDefenseandBuffs)
                                    multiplier = players[playerTurn].DestroyAllBuffs();
                                else
                                    multiplier = players[playerTurn].CheckBuffs(cardToUseObject.GetCardType());
                                if (checkEnemyBuffs)
                                    multiplier *= players[otherPlayer].CheckBuffs(cardToUseObject.GetCardType());
                            }

                            float divider = 1;
                            if (!ignoreDefenses)
                            {
                                if (checkAllDefenseandBuffs)
                                    divider = players[otherPlayer].DestroyAllDefenses();
                                else
                                    divider = players[otherPlayer].CheckDefense(cardToUseObject.GetCardType());
                            }

                            optionalNum = multiplier * divider;
                            ignoreDefenses = false;
                            ignoreBuffs = false;
                            checkEnemyBuffs = false;
                            checkAllDefenseandBuffs = false;

                            if (multiplier != 1 && divider != 1)
                            {
                                if (divider < 1)
                                {
                                    players[otherPlayer].GetCharacter().PlayVoiceLine(Character.VoiceLines.defendAttack);
                                }

                                yield return new WaitForSeconds(1);
                            }
                            players[playerTurn].GetCharacter().PlayVoiceLine(Character.VoiceLines.attack);

                        }
                        else if (cardToUse.card is HealCard)
                        {
                            HealCard h = (HealCard)cardToUse.card;

                            if (CardEffectManager.Instance.CheckActiveEffect(CardEffectEnum.boombox, players[playerTurn].playerNum))
                            {
                                healForEndOfRound[playerTurn] += cardToUse.card.num / 2;
                            }

                            optionalNum = players[playerTurn].CheckBuffs(cardToUseObject.GetCardType());
                            if (optionalNum != 1)
                                yield return new WaitForSeconds(1);

                            players[playerTurn].GetCharacter().PlayVoiceLine(Character.VoiceLines.heal);
                        }

                        //Activate card
                        cardToUse.card.Activate(players[playerTurn], players[otherPlayer], cardToUseObject, optionalNum);
                    }

                    if (CardEffectManager.Instance.CheckActiveEffect(CardEffectEnum.baseballGlove, otherPlayer) &&
                        cardToUseObject.GetCardType() == Card.Type.THROW && cardToUse.card is AttackCard)
                    {
                        players[otherPlayer].GetHand().AddToHand(cardToUse.card, cardToUseObject.GetCost(), cardToUseObject.GetNum(), cardToUseObject.GetCardType());
                    }

                }
                else
                {
                    players[playerTurn].NotEnoughStrength();
                    players[playerTurn].GetCharacter().PlayVoiceLine(Character.VoiceLines.lowStrength);
                }

                //Remove the card
                players[playerTurn].GetHand().cardGrid.RemoveObj(cardToUse.cardObj);
            }

            if(swapPlayersHealth)
            {
                int health1 = players[0].GetCurrentHealth();
                int health2 = players[1].GetCurrentHealth();
                players[0].SetHealth(health2);
                players[1].SetHealth(health1);
                swapPlayersHealth = false;
                yield return new WaitForSeconds(1);
            }

            while (players[playerTurn].GetCharacter().CheckIfVoiceLineIsPlaying())
            {
                yield return new WaitForEndOfFrame();
            }

            //Switch the turn to the next player
            SwitchTurn();
            //Only do this if this isn't the last card
            //if(i + 1 < 5 * players.Length)
            if(!players[0].GetHand().IsHandEmpty() || !players[1].GetHand().IsHandEmpty())
                StartCoroutine(MovePlayerUIForTurn());
        }

        yield return new WaitForSeconds(1f);
        ExpandAndRetractPlayers(true);

        bool flag = false;
        //This is for the boomerang THROW card
        for (int i = 0; i < 2; i++)
        {
            int otherPlayer = (i - 1) * -1;
            if (damageForEndOfRound[i] > 0)
            {
                flag = true;
                players[i].GetCharacter().PlayVoiceLine(Character.VoiceLines.attack);
                float multiplier = players[i].CheckBuffs(Card.Type.THROW);
                float divider = players[otherPlayer].CheckDefense(Card.Type.THROW);
                float optionalNum = multiplier * divider;
                yield return new WaitForSeconds(1);
                int damage = Mathf.RoundToInt(damageForEndOfRound[i] * optionalNum);
                players[otherPlayer].EditHealth(-damage, false);
                damageForEndOfRound[i] = 0;
            }

            if (healForEndOfRound[i] > 0)
            {
                flag = true;
                players[i].GetCharacter().PlayVoiceLine(Character.VoiceLines.heal);
                float multiplier = players[i].CheckBuffs(Card.Type.THROW);
                yield return new WaitForSeconds(1);
                int heal = Mathf.RoundToInt(healForEndOfRound[i] * multiplier);
                players[i].EditHealth(heal, true);
                healForEndOfRound[i] = 0;
            }
        }

        if (flag)
            yield return new WaitForSeconds(1);

        ChangePhase();
    }

    //Gives a card to the player whose turn it is
    public void GiveCardToPlayer(Card card, int editedCost, float editedNum, Card.Type editedType)
    {
        int otherPlayer = (playerTurn - 1) * -1;
        CardObject cardToUseObject = players[playerTurn].GetHand().AddToHand(card, editedCost, editedNum, editedType);
        //Activate the card draw effect
        CardEffect effect = cardToUseObject.GetEffect();
        if (effect != null)
            effect.ActivateOnDraw(players[playerTurn], players[otherPlayer], cardToUseObject);

        SwitchTurn();
        if (!ChangePhase())
        {
            if(wildFire != -1)
            {
                for(int i = 0; i < middleHand.transform.childCount; i++)
                {
                    middleHand.transform.GetChild(i).GetComponent<CardObject>().SetCard(card, editedCost, editedNum, editedType);
                }

                wildFire = -1;
            }

            ExpandAndRetractPlayers();
        }
        else
            ExpandAndRetractPlayers(true);
    }

    IEnumerator MovePlayerUIForTurn()
    {
        int otherPlayer = (playerTurn - 1) * -1;

        yield return new WaitForSeconds(1);

        playerUI[otherPlayer].SetTrigger("Backward");
        playerUI[playerTurn].SetTrigger("Forward");
        ExpandAndRetractPlayers();
    }

    void ExpandAndRetractPlayers(bool retractAll = false)
    {
        int otherPlayer = (playerTurn - 1) * -1;
        if (retractAll)
        {
            players[playerTurn].RetractCharacterImage();
            BackgroundManager.Instance.MoveBackgroundMiddle();
        }
        else
        {
            players[playerTurn].ExpandCharacterImage();
            if (playerTurn == 0)
                BackgroundManager.Instance.MoveBackgroundRight();
            else
                BackgroundManager.Instance.MoveBackgroundLeft();
        }

        players[otherPlayer].RetractCharacterImage();

        if(phase == Phase.pickCards && !retractAll)
        {
            if(players[playerTurn].GetSpecialAbility().CheckSpecialAbility())
                players[playerTurn].GetCharacter().PlayVoiceLine(Character.VoiceLines.charged);
            else
                players[playerTurn].GetCharacter().PlayVoiceLine(Character.VoiceLines.pickTurn);
        }
    }

    BoostObject CreateNewBuff(CardObject card, Player player)
    {
        GameObject g = player.GetHand().boostGrid.CreateCard(buffPrefab);
        BoostObject b = g.GetComponent<BoostObject>();
        b.SetData(card.GetCardType().ToString(), "x" + card.GetNum(), card.cardDescription.text, (player.playerNum == 0));
        return b;
    }

    BoostObject CreateNewDefense(CardObject card, Player player)
    {
        GameObject g = player.GetHand().boostGrid.CreateCard(defensePrefab);
        BoostObject b = g.GetComponent<BoostObject>();
        b.SetData(card.GetCardType().ToString(), "x" + card.GetNum(), card.cardDescription.text, (player.playerNum == 0));
        return b;
    }

    //This will get called by each card when it is played. Its checks if it should change the phase and if it should, then it does
    public bool ChangePhase()
    {
        if(CheckPhaseChange())
        {
            if (phase == Phase.pickCards)
                PlayCardPhase();
            else
                DrawCardsForRound();

            return true;
        }
        return false;
    }

    //Returns the current phase of the card game
    public Phase GetPhase()
    {
        return phase;
    }

    //Checks if the phase should change based on what phase we are currently in
    public bool CheckPhaseChange()
    {
        if(phase == Phase.pickCards)
        {
            //<= 1 because this function will get card before the last card is deleted
            if (middleHand.transform.childCount <= 1)
                return true;

            return false;
        }
        else
        {
            for(int i = 0; i < players.Length; i++)
            {
                if (players[i].GetHand().cardGrid.transform.childCount > 0)
                    return false;
            }

            return true;
        }
    }

    public void WildFirePlayed(int playerNum)
    {
        wildFire = playerNum;
    }

    public int GetPlayerTurn()
    {
        return playerTurn;
    }

    public Player GetCurrentPlayer()
    {
        return players[playerTurn];
    }

    public void ChangeScene(int sceneNum)
    {
        SceneManager.LoadScene(sceneNum);
    }

    public void EndGame(int playerNumThatDied)
    {
        int winningPlayer = (playerNumThatDied - 1) * -1;
        endScreen.EndGame(players[winningPlayer], players[playerNumThatDied]);
        StopAllCoroutines();
    }

    public void SetIgnoreBuffs()
    {
        ignoreBuffs = true;
    }

    public void SetIgnoreDefenses()
    {
        ignoreDefenses = true;
    }

    public void SetCheckEnemyBuffs()
    {
        checkEnemyBuffs = true;
    }

    public void GiveBaseballCard(Player player)
    {
        player.GetHand().AddToHand(baseballCard, 0, 4, Card.Type.THROW);
    }

    public void SwapPlayersHealth()
    {
        swapPlayersHealth = true;
    }

    public void CheckAllDefensesAndBuffs()
    {
        checkAllDefenseandBuffs = true;
    }
}
