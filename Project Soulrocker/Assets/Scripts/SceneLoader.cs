﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneLoader : MonoBehaviour
{
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void ChooseCharacter(int characterNum)
    {
        CharacterChooser.ChooseCharacter(characterNum);
    }

    public void GoToDeckScene()
    {
        SceneManager.LoadScene(2);
    }

    public void GoToCharacterChooseScene()
    {
        SceneManager.LoadScene(0);
    }
}
