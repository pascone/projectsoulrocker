﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deck : MonoBehaviour
{
    public Card[] storedCards;      //All the cards in the deck. This array should not be edited unless the deck is edited

    List<Card> cards = new List<Card>();               //All the cards that are still in the deck. Will be edited as cards get drawn.

    private void Awake()
    {
        if(SavedDeck.GetSavedDeck() != null)
        {
            storedCards = SavedDeck.GetSavedDeck();
        }
    }

    //Shuffles the cards list
    public void Shuffle()
    {
        for (int i = 0; i < cards.Count; i++)
        {
            Card temp = cards[i];
            int randomIndex = Random.Range(i, cards.Count);
            cards[i] = cards[randomIndex];
            cards[randomIndex] = temp;
        }
    }

    //Refills the cards list with all the cards from the storedCards array
    public void Populate()
    {
        cards.Clear();
        foreach (Card c in storedCards)
            cards.Add(c);

        Shuffle();
    }

    //Draws a card from the cards list and removes it from that list
    public Card DrawCard()
    {
        if (cards.Count == 0)
            Populate();

        int index = Random.Range(0, cards.Count);
        Card c = cards[index];
        cards.RemoveAt(index);
        return c;
    }
}
