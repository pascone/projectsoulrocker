﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BicycleEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //This buff can also be used by your opponent

        enemy.AddEnemyBuffICanUse(boostCreated);
    }
    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
