﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShovelEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //If you have at least one of every card style in your hand, then deal double damage

        if(me.GetHand().CheckIfYouHaveOneOfEveryStyleInYourHand())
        {
            cardBeingUsed.EditNum(cardBeingUsed.GetNum() * 2);
        }
    }

    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
