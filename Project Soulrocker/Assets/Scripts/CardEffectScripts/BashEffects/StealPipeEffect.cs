﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StealPipeEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //Swap your strength with your opponent's strength

        int myStrength = me.GetStrength();
        int enemyStrength = enemy.GetStrength();
        int myDifference = enemyStrength - myStrength;
        int enemyDifference = myStrength - enemyStrength;
        me.EditStrength(myDifference);
        enemy.EditStrength(enemyDifference);
    }

    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
