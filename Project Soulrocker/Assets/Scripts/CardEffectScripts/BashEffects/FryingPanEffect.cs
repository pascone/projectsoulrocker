﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FryingPanEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //Will destroy ALL buffs on the field

        me.DestroyAllBuffs();
        enemy.DestroyAllBuffs();
    }

    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
