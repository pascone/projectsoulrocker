﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleWrapEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //While this is on the field, all healing is nullified

        CardEffectManager.Instance.ActivateEffect(CardEffectEnum.bubbleWrap, -1);
        boostCreated.BubbleWrapObject(me);
    }

    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
