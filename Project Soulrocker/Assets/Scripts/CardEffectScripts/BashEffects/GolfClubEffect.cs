﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolfClubEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //Will destroy ALL defenses on the field

        me.DestroyAllDefenses();
        enemy.DestroyAllDefenses();
    }

    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
