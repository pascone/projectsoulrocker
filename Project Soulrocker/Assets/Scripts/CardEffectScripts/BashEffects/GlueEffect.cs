﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlueEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //While this is on the field, your defenses will not be destroyed after use.

        CardEffectManager.Instance.ActivateEffect(CardEffectEnum.bananaPeel, me.playerNum);
        me.DontDestroyDefenses(boostCreated);
        boostCreated.GlueObject(me);
    }

    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
