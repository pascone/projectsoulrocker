﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseballBatEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //Uses all of your strength and will deal that amount x3

        int currStrenght = me.GetStrength();
        int damageAmount = currStrenght * 3;
        cardBeingUsed.EditNum(damageAmount);
        me.EditStrength(-currStrenght);
    }

    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
