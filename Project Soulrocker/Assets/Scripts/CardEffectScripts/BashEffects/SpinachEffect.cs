﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinachEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //Does not heal you at all, but it increases your max health by 50 

        me.EditMaxHealth(50);
    }

    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}