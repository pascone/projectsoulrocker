﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadbuttEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //Swaps health with your opponent after this heals you

        CardGameManager.Instance.SwapPlayersHealth();
    }
    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}