﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodenPlankEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //If the next card you play is a BASH card, then you will gain 2 strength.

        CardEffectManager.Instance.ActivateEffect(CardEffectEnum.woodenPlank, me.playerNum);
    }
    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
