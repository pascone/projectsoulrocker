﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisinfectantEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //Ignores any buffs

        CardGameManager.Instance.SetIgnoreBuffs();
    }

    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}