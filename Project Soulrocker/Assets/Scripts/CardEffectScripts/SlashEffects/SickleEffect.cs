﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SickleEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //Heal for the amount of damage you deal

        CardEffectManager.Instance.ActivateEffect(CardEffectEnum.sickle, me.playerNum);
    }

    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
