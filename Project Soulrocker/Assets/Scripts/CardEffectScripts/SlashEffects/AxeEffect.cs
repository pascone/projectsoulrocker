﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxeEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //Ignores any defenses

        CardGameManager.Instance.SetIgnoreDefenses();
    }

    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
