﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BananaPeelEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //While this is on the field, all healing deals damage instead.

        CardEffectManager.Instance.ActivateEffect(CardEffectEnum.bananaPeel, -1);
        boostCreated.BananaPeelObject();
    }

    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
