﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuctTapeEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //All your buffs on the field, no matter their Style, will get used to make this buff stronger

        float num = me.DestroyAllBuffs(boostCreated);
        boostCreated.EditMultiplier(num, Color.yellow);
    }

    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
