﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PencilEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //If the next card you play is a SLASH card, then it will be played twice

        CardEffectManager.Instance.ActivateEffect(CardEffectEnum.pencil, me.playerNum);
    }

    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
