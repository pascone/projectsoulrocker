﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PizzaCutterEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //Heal your opponent for 5

        enemy.EditHealth(5, true);
    }

    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
