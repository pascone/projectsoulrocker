﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyDrinkEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {

    }

    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
        //This card will always be the first one used if it is in your hand unless your special ability overrides it.

        int index = me.GetHand().cardGrid.transform.childCount - 1;
        index = 4 - index;
        me.GetHand().SetEnergyDrink(index);
    }
}

