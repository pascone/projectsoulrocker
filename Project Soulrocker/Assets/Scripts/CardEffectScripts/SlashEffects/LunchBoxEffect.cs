﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LunchBoxEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //All additional damage gotten from this buff will heal you instead

        CardEffectManager.Instance.ActivateEffect(CardEffectEnum.lunchBox, me.playerNum);
    }
    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
