﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivestreamEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //Reduce your max health by 25 and fully heal yourself

        me.EditMaxHealth(-25);
        int max = me.GetMaxHealth();
        int curr = me.GetCurrentHealth();

        int num = max - curr;

        if (num < 0)
            num = 0;

        me.EditHealth(num, true);
    }

    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}