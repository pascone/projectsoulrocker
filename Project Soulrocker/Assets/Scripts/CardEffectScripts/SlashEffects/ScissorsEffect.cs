﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScissorsEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //Steal a random card from your opponent and put it in your hand

        CardInfo cardToSteal = enemy.GetHand().UseRandomCard(true);
        if (cardToSteal.card != null)
        {
            CardObject cardObject = cardToSteal.cardObj.GetComponent<CardObject>();
            enemy.GetHand().cardGrid.RemoveObj(cardToSteal.cardObj);
            me.GetHand().AddToHand(cardToSteal.card, cardObject.GetCost(), cardObject.GetNum(), cardObject.GetCardType());
        }
    }

    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
