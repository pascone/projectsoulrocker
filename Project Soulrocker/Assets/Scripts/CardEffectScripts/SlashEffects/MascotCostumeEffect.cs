﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MascotCostumeEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //While this is on the field, all buffs do 1.5x more damage

        CardEffectManager.Instance.ActivateEffect(CardEffectEnum.mascotCostume, me.playerNum);
        boostCreated.MascotCostume(me);
    }
    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
