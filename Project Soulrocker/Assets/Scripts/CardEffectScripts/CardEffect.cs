﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CardEffect
{
    public abstract void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated);
    public abstract void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed);
}
