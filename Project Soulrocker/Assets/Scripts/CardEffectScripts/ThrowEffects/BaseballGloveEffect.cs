﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseballGloveEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //Catch the next THROW attack that is played by your opponent and put it in your hand.

        CardEffectManager.Instance.ActivateEffect(CardEffectEnum.baseballGlove, me.playerNum);
        boostCreated.BaseballGlove(me);
    }
    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
