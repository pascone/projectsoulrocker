﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncenseEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //Deals 10 damage to you first, then heals you for 20

        me.EditHealth(-10, false);
    }

    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
