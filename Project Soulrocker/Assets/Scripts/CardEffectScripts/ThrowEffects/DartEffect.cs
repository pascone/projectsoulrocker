﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DartEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //This attack will use any THROW buffs that your opponent has

        CardGameManager.Instance.SetCheckEnemyBuffs();
    }

    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
