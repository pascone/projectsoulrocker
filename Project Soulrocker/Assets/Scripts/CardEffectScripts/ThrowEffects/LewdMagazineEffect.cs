﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LewdMagazineEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //Heals you and your opponent

        enemy.EditHealth((int)cardBeingUsed.GetNum(), true);
    }

    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
