﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlassBeakerEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //Change the next card you play's style to THROW

        CardEffectManager.Instance.ActivateEffect(CardEffectEnum.glassBeaker, me.playerNum);
    }
    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
