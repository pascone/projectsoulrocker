﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomboxEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //Use half the heal now and half at the end of the round

        CardEffectManager.Instance.ActivateEffect(CardEffectEnum.boombox, me.playerNum);
    }
    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
