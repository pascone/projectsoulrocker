﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BagOfMarblesEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //Change the style of every defense and buff on the field to THROW

        me.ChangeAllBoostStyle(Card.Type.THROW);
        enemy.ChangeAllBoostStyle(Card.Type.THROW);
    }
    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
