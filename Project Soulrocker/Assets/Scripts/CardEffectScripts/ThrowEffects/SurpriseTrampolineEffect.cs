﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurpriseTrampolineEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //The next THROW attack dealt to you is also dealt to your opponent

        CardEffectManager.Instance.ActivateEffect(CardEffectEnum.surpriseTrampoline, me.playerNum);
        boostCreated.SurpriseTrampoline(me);
    }
    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
