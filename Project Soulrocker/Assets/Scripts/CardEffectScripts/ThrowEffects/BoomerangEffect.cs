﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomerangEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //Deal half the damage now and half at the end of the round

        CardEffectManager.Instance.ActivateEffect(CardEffectEnum.boomerang, me.playerNum);
    }
    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
