﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikedBaseballEffect : CardEffect
{
    public override void Activate(Player me, Player enemy, CardObject cardBeingUsed, BoostObject boostCreated)
    {
        //Give your opponent a 0 | 4 THROW card

        CardGameManager.Instance.GiveBaseballCard(enemy);
    }

    public override void ActivateOnDraw(Player me, Player enemy, CardObject cardBeingUsed)
    {
    }
}
