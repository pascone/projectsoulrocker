﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SplashManager : MonoBehaviour
{
    #region SINGLETON PATTERN
    public static SplashManager _instance;
    public static SplashManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<SplashManager>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("SplashManager");
                    _instance = container.AddComponent<SplashManager>();
                }
            }

            return _instance;
        }
    }
    #endregion

    [System.Serializable]
    public struct PlayerSpecificSplash
    {
        public TextMeshProUGUI damageText;
        public TextMeshProUGUI healText;
        public Animator damageAnim;
        public Animator healAnim;
    }

    public Animator animator;
    public TextMeshProUGUI splashText;
    public Image bannerBacker;
    public PlayerSpecificSplash[] playerSpecificSplashes;
    bool splashInProgress = false;

    public void CreateSplash(string text, Color backerColor, Color textColor)
    {
        splashText.text = text;
        bannerBacker.color = backerColor;
        splashText.color = textColor;
        animator.SetTrigger("Splash");
    }

    public void HealSplash(int playerNum, int healAmount)
    {
        StartCoroutine(DoHealSplash(playerNum, healAmount));
    }

    public void DamageSplash(int playerNum, int damageAmount)
    {
        int num = Mathf.Abs(damageAmount);
        StartCoroutine(DoDamageSplash(playerNum, num));
    }

    IEnumerator DoHealSplash(int playerNum, int healAmount)
    {
        while (splashInProgress)
            yield return new WaitForEndOfFrame();

        splashInProgress = true;
        playerSpecificSplashes[playerNum].healText.text = healAmount.ToString();
        playerSpecificSplashes[playerNum].healAnim.SetTrigger("Heal");

        yield return new WaitForSeconds(1);
        splashInProgress = false;
    }

    IEnumerator DoDamageSplash(int playerNum, int num)
    {
        while (splashInProgress)
            yield return new WaitForEndOfFrame();

        splashInProgress = true;
        playerSpecificSplashes[playerNum].damageText.text = num.ToString();
        playerSpecificSplashes[playerNum].damageAnim.SetTrigger("Damage");

        yield return new WaitForSeconds(1);
        splashInProgress = false;
    }
}
